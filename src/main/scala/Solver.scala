object Solver extends App {

	// Generate a new sudoku board from string.
	def parse(str: String) = {
  	// Generate key->value for initial unfiltered map representing a board.
		val keys = (for (row <- 0 to 8; col <- 0 to 8) yield (row, col)) toList
		val values = (for (v <- str.toList) yield {
  		if (v == '.') (1 to 9).toList
  		else List(v.asDigit)
  	}) toList

		// (row, col) -> legal placements at (row, col)
  	val unfilteredMap = (keys zip values) toMap

  	// Generate list of coordinates that need their peers updated.
  	val toUpdate = ((for (i <- 0 to 8; j <- 0 to 8 if (unfilteredMap((i,j)).length == 1)) 
  		yield { ((i,j), unfilteredMap(i,j).head)}) toList)

  	new Board(buildStartMap(toUpdate, unfilteredMap))  
	}

	// Filter all peers of initial placements in map for a new board. 
	def buildStartMap(lst: List[((Int, Int), Int)], m: Map[(Int, Int), List[Int]]): Map[(Int, Int), List[Int]] = 
		lst match {
  		case Nil => m 
  		case head::tail => buildStartMap(lst.tail, filterPeers(m, (peers(head._1._1, head._1._2)), head._2)) 
  	}
	// Update the row, col and box of all singletons (placed numbers)
	def filterPeers(m: Map[(Int, Int), List[Int]], keys: List[(Int, Int)], n: Int): Map[(Int, Int), List[Int]] = keys match {
		case Nil => m
		case head::tail => filterPeers((m updated (head, (m(head) filter (x => x != n)))), tail, n) 
	}

	/* Returns a list of peers to the coordinate (row, col) */
	def peers(row: Int, col: Int): List[(Int, Int)] = {
  	val rowStart = row - (row % 3) 
    val colStart = col - (col % 3)

    val box = (for (i <- rowStart to rowStart + 2 ; j <- colStart to colStart + 2)
    	yield (i, j)).toList

    val unfiltered = 
    	((for (i <- 0 to 8) yield (row, i)) ++ (for (j <- 0 to 8) yield (j, col))++ box).toList

    (unfiltered filter (x => x != (row, col))).toSet.toList
	  }
}


class Board(val available: Map[(Int, Int), List[Int]]) {

	def availableValuesAt(row: Int, col: Int): List[Int] = {
    available.getOrElse((row, col), 1.to(9).toList)
  }

  def valueAt(row: Int, col: Int): Option[Int] = {
    available get (row, col) match {
      case Some(x) => if (x.length == 1) Some(x.head) else None
      case None => None
    }
  }

  // solved if every board has a value
  def isSolved(): Boolean = {
    for (i <- 0 to 8; j <- 0 to 8 if (valueAt(i,j) == None)) return false 
    return true
  }

  def isUnsolvable(): Boolean = {
    for (i <- 0 to 8; j <- 0 to 8) 
      if (availableValuesAt(i,j).length == 0) return true
    false
  }

  def place(row: Int, col: Int, value: Int): Board = {
    require(availableValuesAt(row, col).contains(value))
    val pMap = available updated((row, col), List(value))
    val peersLst = Solver.peers(row, col) filter (x => pMap(x).contains(value))

    //take the map, take the list of peers that need update, the value) 
    def helper(m: Map[(Int, Int), List[Int]], pList: List[(Int, Int)], n: Int): Map[(Int, Int), List[Int]] = pList match {
      case Nil => m
      case head::tail => {
        val updatedMap = m updated (pList.head, ((m(pList.head._1, pList.head._2)) filter (x => x != n)))
        // if an updated mapping became a singleton, update its peers too
        if (updatedMap(pList.head._1, pList.head._2).length == 1) 
          helper(
            helper(updatedMap, (Solver.peers(pList.head._1, pList.head._2) filter (x => updatedMap(x._1, x._2) contains(updatedMap(pList.head._1, pList.head._2).head))),
              updatedMap(pList.head._1, pList.head._2).head), tail, n) 
        else helper(updatedMap, tail, n) 
      }
   } 
     new Board(helper(pMap, peersLst, value))
  }
  // You can return any Iterable (e.g., Stream)
  def nextStates(): List[Board] = {
    if (isUnsolvable()) {
      return List()
    }
    else {
      // for mappings that aren't restricted to 1 value
      // for each value in those mappings
      // place the value and yield the board 
      def valueCount(board: Board): Int = {
        val count = (for (i <- 0 to 8; j <- 0 to 8 if (board.available(i,j).length > 1)) yield (board.available(i,j).length-1)) toList

        count.foldLeft(0)(_ + _)
       }

      val nextMovesCount = (for (i <- 0 to 8; j <- 0 to 8 if (available(i,j).length > 1)) yield {
        for (value <- available(i,j)) yield {
          (place(i,j, value), valueCount(place(i,j, value)))
        }
      } ) toList

      val tups = nextMovesCount.flatten.sortWith((x,y) => y._2 > x._2 )      
      return (for (v <- tups) yield v._1) toList
    }
  }

  def solve(): Option[Board] = {
    if (this.isSolved) {
      return Some(this)
     }
    else { 
      for (boards <- (this.nextStates)) {
        boards.solve match {
          case Some(x) => return Some(x) 
          case None =>
        }
        None
      }
    } 
    return None
  }
  
  override def toString(): String = {
    var str: String = ""
    for(i <- 0 to 8){
  		if(i == 0 || i == 3 || i == 6) str = str + "-------------\n"
    		for(j <- 0 to 8){
      			if(j == 0 || j == 3 || j == 6) str = str + "|"
          			val ls:List[Int] = available((i, j))
          		if(ls.size == 1) str = str + ls(0)
          		else str = str + "."
          		if(j == 8) str = str + "|"
    		}
        	str = str + "\n"
        if(i == 8) str = str + "-------------\n"
      }
      return str
  }
}