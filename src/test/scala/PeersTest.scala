import Solver._

class PeersTest extends org.scalatest.FunSuite {
	test("peers returns All peers NOT (r, c)") {
		val lstRow = (for(row <- 0 to 8) yield (2, row)).toList
		val lstCol = (for(col <- 0 to 8) yield (col, 2)).toList	
		val testLst = (lstRow++lstCol++List((0,0), (0,1), (1,0), (1,1))) filter (x => x != (2,2))
		assert(peers(2,2).toSet == testLst.toSet)
	}
}