import Solver._

class PhaseThreeTestSuite extends org.scalatest.FunSuite {
		// UNSOLVABLE BOARDS 
		val puz = "...8...42.....1.7.9....2..3..7....5.1...3...4.4....8..4..9....6.5.2...1.28...6..."
		val puz1 = ".43.8.25.6.............1.949....4.7....6.8....1.2....382.5.............5.34.9.71."
		val fromCS121_1 = "85....4.1......67...21....3..85....7...982...3....15..5....43...37......2.9....58"
		val e27 = "....4.....58.1..76.79.6..21..4..32.8...4.2...3.76..9..54..2.79.76..3.84.....5...."
		
		// SOLVABLE BOARDS
		val puz2 = "2...8.3...6..7..84.3.5..2.9...1.54.8.........4.27.6...3.1..7.4.72..4..6...4.1...3"
		val ezPuz2 = "24598137616927..84.3.5..2.9...1.54.8.........4.27.6...3.1..7.4.72..4..6...4.12793"
		val fromCS121_2 = ".1.....2..3..9..1656..7...33.7..8..........89....6......6.254..9.5..1..7..3.....2"
		val puzzle = "....8.3...6..7..84.3.5..2.9...1.54.8.........4.27.6...3.1..7.4.72..4..6...4.1...3"

		test("nextStates") {
			// ALL nextStates even illegal states ordered by moves
			//println(parse(puz2).nextStates)
		}
		test("Solve on puz2"){
			val Some(p) = parse(puz2).solve
			assert(p.isSolved == true)
		}

		test("Solve on ezPuz2") {
			val Some(p) = parse(ezPuz2).solve
			assert(p.isSolved == true)
		}

		test("Solve on fromCS121_2") {
			val Some(p) = parse(fromCS121_2).solve
			assert(p.isSolved == true)
		}

		test("Solve on puzzle") {
			val Some(p) = parse(puzzle).solve
			assert(p.isSolved == true)
		}
		test("Solve hard puzzle") {
			val Some(p) = parse(puz).solve
			assert(p.isSolved == true)
		}
}