import Solver._

class Solve_Place_Test extends org.scalatest.FunSuite { 
	// solvable board
	val puz = "...8...42.....1.7.9....2..3..7....5.1...3...4.4....8..4..9....6.5.2...1.28...6..."
	
	test("isSolved returns true if every cell is constrained to one value") {
		val solvedInstance = new Board((for (i <- 0 to 8; j <- 0 to 8) yield ((i,j) -> List(1))) toMap)
		assert(solvedInstance.isSolved() == true)
	}
	test("isSolved returns false on unsolved board that is solvable") {
		assert(parse(puz).isSolved == false)
	}

	test("isUnsolvable returns true when a cell is restricted to the empty list") {
		// a board containing a mapping to empty list
		val board = new Board(Map((4,2) -> List()))
		assert(board.isUnsolvable == true)
	}
	test("isUnsolvable returns true <=> a cell is restricted to the empty cell"){
		assert(new Board(parse(puz).available updated((0,0), List[Int]())).isUnsolvable == true)
	}

	test("place executes base case properly") { // TEST NEEDS TO BE UPDATED TO REFLECT toString changes.
		val board = new Board(Map())
		val emptyStr = "................................................................................."
		// Expected map after playing a 1 in (0,0) on empty board.
		val expected = new Board(Map((0,0)->List(1), (0,1)->List(2,3,4,5,6,7,8,9), (0,2)->List(2,3,4,5,6,7,8,9),
			(0,3)->List(2,3,4,5,6,7,8,9), (0,4)->List(2,3,4,5,6,7,8,9), (0,5)->List(2,3,4,5,6,7,8,9),
			(0,6)->List(2,3,4,5,6,7,8,9),(0,7)->List(2,3,4,5,6,7,8,9),(0,8)->List(2,3,4,5,6,7,8,9),

			(1,0)->List(2,3,4,5,6,7,8,9),(2,0)->List(2,3,4,5,6,7,8,9),(3,0)->List(2,3,4,5,6,7,8,9),
			(4,0)->List(2,3,4,5,6,7,8,9),(5,0)->List(2,3,4,5,6,7,8,9),(6,0)->List(2,3,4,5,6,7,8,9),
			(7,0)->List(2,3,4,5,6,7,8,9),(8,0)->List(2,3,4,5,6,7,8,9),

			(1,1)->List(2,3,4,5,6,7,8,9), (1,2)->List(2,3,4,5,6,7,8,9),
			(2,1)->List(2,3,4,5,6,7,8,9), (2,2)->List(2,3,4,5,6,7,8,9)
			)) // Change this to for yield apply 
		//println(parse(emptyStr).place(0,0,1))
		//println(expected)
		// assert(parse(emptyStr).place(0,0,1).toString == expected.toString)
	}

	test("constraining place test") {
		val expected1 = 
		"1234567.."+
		"........."+
		"........."+
		"........."+
		"2.4.6.8.7"+
		"........."+
		"........."+
		"347......"+
		"56......."

		var board: Board = parse(
	  	""
	  		+ "331887868"
	  		+ "3.9888.8."
	  		+ "567886828"
	  		+ "888888888"
	  		+ "888888988"
	  		+ "888888888"
	  		+ "888888188"
	  		+ "888888888"
	  		+ "888888588")
		var checkBoard: Board = parse(
			  	""
			  		+ "331887868"
			  		+ "34988878."
			  		+ "567886828"
			  		+ "888888888"
			  		+ "888888988"
			  		+ "888888888"
			  		+ "888888188"
			  		+ "888888888"
			  		+ "888888588")
			assert(checkBoard.available == board.place(1, 1, 4).available)
	}
		test("placed successfully"){
			val emptyBoard = "........." + 
						 	 "........." +
						 	 "........." +
							 "........." + 
							 "........." +
							 "........." +
							 "........." + 
							 "........." +
							 "........."

			val startBoard = "........." + 
						 	 "........." +
						 	 "........." +
							 "........." + 
							 "........." +
							 "........." +
							 "........." + 
							 "........." +
							 "........1"
			val testEmpty = parse(emptyBoard)
			val testStart = parse(startBoard)
			assert(testEmpty.place(8,8,1).toString == testStart.toString)
	}
}